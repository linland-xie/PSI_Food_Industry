关于PSI食品行业版
-------------
>PSI食品行业版是基于开源进销存PSI（ http://git.oschina.net/crm8000/PSI ），深度开发的适用于食品行业的管理软件系统。

PSI食品行业版的开源协议
-------------
>PSI食品行业版的开源协议为GPL v3

如需要技术支持，请联系
-------------
- QQ群： <a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=64808ce24f2a3186ccb1f37aad9ed591bcc4fb257d09749753aca98c6c73e400">414474186</a>

致谢
-------------
>PSI使用了如下开源软件，没有你们，就没有PSI
> 
>1、PHP (http://php.net/)
>
>2、MySQL (http://www.mysql.com/)
>
>3、ExtJS 4.2 (http://www.sencha.com/)
>
>4、ThinkPHP 3.2.3 (http://www.thinkphp.cn/)
>
>5、乱码 / pinyin_php (https://git.oschina.net/cik/pinyin_php)
>
>6、PHPExcel (https://github.com/PHPOffice/PHPExcel)
>
>7、TCPDF (http://www.tcpdf.org/)
>