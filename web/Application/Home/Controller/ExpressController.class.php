<?php

namespace Home\Controller;

use Think\Controller;
use Home\Service\ExpressService;
use Home\Common\FIdConst;
use Home\Service\UserService;

/**
 * 运单 Controller
 *
 * @author 李静波
 *        
 */
class ExpressController extends PSIBaseController {

	/**
	 * 运单管理 - 主页面
	 */
	public function index() {
		$us = new UserService();
		
		if ($us->hasPermission(FIdConst::EXPRESS_BILL_MANAGEMENT)) {
			$this->initVar();
			
			$this->assign("title", "运单管理");
			
			$this->assign("pAdd", 
					$us->hasPermission(FIdConst::EXPRESS_BILL_MANAGEMENT_ADD) ? "1" : "0");
			$this->assign("pEdit", 
					$us->hasPermission(FIdConst::EXPRESS_BILL_MANAGEMENT_EDIT) ? "1" : "0");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/Express/index");
		}
	}

	/**
	 * 寄件
	 */
	public function addExpressBill() {
		if (IS_POST) {
			$params = array(
					"ref" => I("post.ref"),
					"startPlace" => I("post.startPlace"),
					"destPlace" => I("post.destPlace"),
					"recName" => I("post.recName"),
					"recAddress" => I("post.recAddress"),
					"recTel" => I("post.recTel"),
					"customerId" => I("post.customerId"),
					"sendAddress" => I("post.sendAddress"),
					"sendTel" => I("post.sendTel"),
					"cargo" => I("post.cargo"),
					"fragile" => I("post.fragile"),
					"freight" => I("post.freight"),
					"payment" => I("post.payment"),
					"claimValue" => I("post.claimValue"),
					"cargoMoney" => I("post.cargoMoney"),
					"memo" => I("post.memo")
			);
			
			$es = new ExpressService();
			
			$this->ajaxReturn($es->addExpressBill($params));
		}
	}

	/**
	 * 运单列表
	 */
	public function expressList() {
		if (IS_POST) {
			$params = array(
					"billStatus" => I("post.billStatus"),
					"ref" => I("post.ref"),
					"start" => I("post.start"),
					"limit" => I("post.limit")
			);
			
			$es = new ExpressService();
			
			$this->ajaxReturn($es->expressList($params));
		}
	}

	/**
	 * 运单变更详情
	 */
	public function expressTracing() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$es = new ExpressService();
			
			$this->ajaxReturn($es->expressTracing($params));
		}
	}

	/**
	 * 运单变更
	 */
	public function editExpressBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id"),
					"status" => I("post.status"),
					"recName" => I("post.recName"),
					"recAddress" => I("post.recAddress"),
					"recTel" => I("post.recTel"),
					"customerId" => I("post.customerId"),
					"sendAddress" => I("post.sendAddress"),
					"sendTel" => I("post.sendTel"),
					"cargo" => I("post.cargo"),
					"fragile" => I("post.fragile"),
					"freight" => I("post.freight"),
					"payment" => I("post.payment"),
					"claimValue" => I("post.claimValue"),
					"cargoMoney" => I("post.cargoMoney"),
					"memo" => I("post.memo"),
					"ref" => I("post.ref"),
					"startPlace" => I("post.startPlace"),
					"destPlace" => I("post.destPlace")
			);
			$es = new ExpressService();
			
			$this->ajaxReturn($es->editExpressBill($params));
		}
	}

	/**
	 * 获得某个运单的详情
	 */
	public function expressBillInfo() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->expressBillInfo($params));
		}
	}

	/**
	 * 快递公司 - 主页面
	 */
	public function companyIndex() {
		$us = new UserService();
		
		if ($us->hasPermission(FIdConst::EXPRESS_COMPANY)) {
			$this->initVar();
			
			$this->assign("title", "快递公司");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/Express/companyIndex");
		}
	}

	public function companyList() {
		if (IS_POST) {
			$es = new ExpressService();
			$this->ajaxReturn($es->companyList());
		}
	}

	public function editCompany() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id"),
					"name" => I("post.name")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->editCompany($params));
		}
	}

	public function deleteCompany() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->deleteCompany($params));
		}
	}

	public function getExpressBillTemplateInfo() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->getExpressBillTemplateInfo($params));
		}
	}

	public function editExpressBillTemplate() {
		if (IS_POST) {
			$params = array(
					"jsonStr" => I("post.jsonStr")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->editExpressBillTemplate($params));
		}
	}

	public function getTemplateInfo() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->getTemplateInfo($params));
		}
	}

	public function getTemplateInfoForDesign() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->getTemplateInfoForDesign($params));
		}
	}

	public function designExpressBillTemplate() {
		if (IS_POST) {
			$params = array(
					"jsonStr" => I("post.jsonStr")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->designExpressBillTemplate($params));
		}
	}

	public function deleteDesignBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->deleteDesignBill($params));
		}
	}

	/**
	 * 设置发件人信息
	 */
	public function editSenderInfo() {
		if (IS_POST) {
			$params = array(
					"name" => I("post.name"),
					"tel" => I("post.tel"),
					"address" => I("post.address")
			);
			
			$es = new ExpressService();
			$this->ajaxReturn($es->editSenderInfo($params));
		}
	}

	public function getSenderInfo() {
		if (IS_POST) {
			
			$es = new ExpressService();
			$this->ajaxReturn($es->getSenderInfo());
		}
	}
}