<?php

namespace Home\Controller;

use Home\Common\FIdConst;
use Home\Service\UserService;
use Think\Controller;
use Home\Service\ShopService;

/**
 * 店铺 Controller
 *
 * @author 李静波
 *        
 */
class ShopController extends PSIBaseController {

	/**
	 * 店铺管理 - 主页面
	 */
	public function index() {
		$us = new UserService();
		
		if ($us->hasPermission(FIdConst::SHOP_MANAGEMENT)) {
			$this->initVar();
			
			$this->assign("title", "店铺管理");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/Shop/index");
		}
	}

	public function shopList() {
		if (IS_POST) {
			$ss = new ShopService();
			
			$this->ajaxReturn($ss->shopList());
		}
	}

	public function editShop() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id"),
					"code" => I("post.code"),
					"name" => I("post.name"),
					"address" => I("post.address"),
					"masterId" => I("post.masterId"),
					"usePOS" => I("post.usePOS"),
					"warehouseId" => I("post.warehouseId"),
					"shopType" => I("post.shopType")
			);
			
			$ss = new ShopService();
			
			$this->ajaxReturn($ss->editShop($params));
		}
	}

	public function shopInfo() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ss = new ShopService();
			
			$this->ajaxReturn($ss->shopInfo($params));
		}
	}

	public function deleteShop() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ss = new ShopService();
			
			$this->ajaxReturn($ss->deleteShop($params));
		}
	}

	public function selectUsers() {
		if (IS_POST) {
			$idList = I("post.idList");
			
			$ss = new ShopService();
			
			$this->ajaxReturn($ss->selectUsers($idList));
		}
	}

	public function editShopUser() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id"),
					"userIdList" => I("post.userIdList")
			);
			
			$ss = new ShopService();
			
			$this->ajaxReturn($ss->editShopUser($params));
		}
	}

	public function userList() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ss = new ShopService();
			
			$this->ajaxReturn($ss->userList($params));
		}
	}

	public function userListForEdit() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ss = new ShopService();
			
			$this->ajaxReturn($ss->userListForEdit($params));
		}
	}
}