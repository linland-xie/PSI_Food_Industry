/**
 * 条码 - 新增或编辑界面
 */
Ext.define("PSI.Barcode.EditForm", {
			extend : "Ext.window.Window",

			config : {
				parentForm : null,
				entity : null
			},

			/**
			 * 初始化组件
			 */
			initComponent : function() {
				var me = this;

				var entity = me.getEntity();

				me.adding = entity == null;

				var buttons = [];
				if (!entity) {
					var btn = {
						text : "保存并继续新增",
						formBind : true,
						handler : function() {
							me.onOK(true);
						},
						scope : me
					};

					buttons.push(btn);
				}

				var btn = {
					text : "保存",
					formBind : true,
					iconCls : "PSI-button-ok",
					handler : function() {
						me.onOK(false);
					},
					scope : me
				};
				buttons.push(btn);

				var btn = {
					text : entity == null ? "关闭" : "取消",
					handler : function() {
						me.close();
					},
					scope : me
				};
				buttons.push(btn);

				Ext.apply(me, {
							title : entity == null ? "新增条码" : "编辑条码",
							modal : true,
							resizable : false,
							onEsc : Ext.emptyFn,
							width : 400,
							height : 110,
							layout : "fit",
							listeners : {
								show : {
									fn : me.onWndShow,
									scope : me
								},
								close : {
									fn : me.onWndClose,
									scope : me
								}
							},
							items : [{
								id : "editForm_Barcode",
								xtype : "form",
								layout : {
									type : "table",
									columns : 1
								},
								height : "100%",
								bodyPadding : 5,
								defaultType : 'textfield',
								fieldDefaults : {
									labelWidth : 40,
									labelAlign : "right",
									labelSeparator : "",
									msgTarget : 'side',
									width : 370,
									margin : "5"
								},
								items : [{
									xtype : "hidden",
									name : "id",
									value : entity == null ? null : entity
											.get("id")
								}, {
									id : "editCode_Barcode",
									fieldLabel : "条码",
									allowBlank : false,
									blankText : "没有输入条码",
									beforeLabelTextTpl : PSI.Const.REQUIRED,
									name : "code",
									value : entity == null ? null : entity
											.get("barcode"),
									listeners : {
										specialkey : {
											fn : me.onEditCodeSpecialKey,
											scope : me
										}
									}
								}],
								buttons : buttons
							}]
						});

				me.callParent(arguments);
			},

			/**
			 * 保存
			 */
			onOK : function(thenAdd) {
				var me = this;
				var f = Ext.getCmp("editForm_Barcode");
				var el = f.getEl();
				el.mask(PSI.Const.SAVING);
				var sf = {
					url : PSI.Const.BASE_URL + "Home/Barcode/editBarcode",
					method : "POST",
					success : function(form, action) {
						me.__lastId = action.result.id;

						el.unmask();

						PSI.MsgBox.tip("数据保存成功");
						me.focus();
						if (thenAdd) {
							me.clearEdit();
						} else {
							me.close();
						}
					},
					failure : function(form, action) {
						el.unmask();
						PSI.MsgBox.showInfo(action.result.msg, function() {
									Ext.getCmp("editCode_Barcode").focus();
								});
					}
				};
				f.submit(sf);
			},

			onEditCodeSpecialKey : function(field, e) {
				var me = this;

				if (e.getKey() == e.ENTER) {
					var f = Ext.getCmp("editForm_Barcode");
					if (f.getForm().isValid()) {
						me.onOK(me.adding);
					}
				}
			},

			clearEdit : function() {
				Ext.getCmp("editCode_Barcode").focus();

				var editors = [Ext.getCmp("editCode_Barcode")];
				for (var i = 0; i < editors.length; i++) {
					var edit = editors[i];
					edit.setValue(null);
					edit.clearInvalid();
				}
			},

			onWndClose : function() {
				var me = this;
				if (me.__lastId) {
					if (me.getParentForm()) {
						me.getParentForm().refreshMainGrid(me.__lastId);
					}
				}
			},

			onWndShow : function() {
				var editCode = Ext.getCmp("editCode_Barcode");
				editCode.focus();
				editCode.setValue(editCode.getValue());
			}
		});