/**
 * 自定义字段 - 商品字段
 */
Ext.define("PSI.Goods.GoodsField", {
	extend : "Ext.form.field.Trigger",
	alias : "widget.psi_goods_with_inventory_field",

	config : {
		parentCmp : null,
		editWarehouseName : null
	},

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		me.enableKeyEvents = true;

		me.callParent(arguments);

		me.on("keydown", function(field, e) {
					if (e.getKey() == e.BACKSPACE) {
						field.setValue(null);
						e.preventDefault();
						return false;
					}

					if (e.getKey() != e.ENTER && !e.isSpecialKey(e.getKey())) {
						this.onTriggerClick(e);
					}
				});
	},

	onTriggerClick : function(e) {
		var me = this;
		var modelName = "PSIGoodsWithInventoryField";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "goodsId", "code", "name", "spec", "unitName",
							"warehouseName", "goodsCount", "qcBeginDT",
							"qcEndDT", "expiration", "goodsMoney", "goodsPrice"]
				});

		var store = Ext.create("Ext.data.Store", {
					model : modelName,
					autoLoad : false,
					data : []
				});
		var lookupGrid = Ext.create("Ext.grid.Panel", {
					columnLines : true,
					border : 0,
					store : store,
					columns : [{
								header : "仓库",
								dataIndex : "warehouseName",
								menuDisabled : true
							}, {
								header : "编码",
								dataIndex : "code",
								menuDisabled : true,
								width : 70
							}, {
								header : "商品",
								dataIndex : "name",
								menuDisabled : true,
								flex : 1
							}, {
								header : "规格型号",
								dataIndex : "spec",
								menuDisabled : true,
								flex : 1
							}, {
								header : "生产日期",
								dataIndex : "qcBeginDT",
								menuDisabled : true,
								width : 80
							}, {
								header : "保质期(天)",
								dataIndex : "expiration",
								menuDisabled : true,
								width : 80
							}, {
								header : "到期日期",
								dataIndex : "qcEndDT",
								menuDisabled : true,
								width : 80
							}, {
								header : "当前可用库存",
								dataIndex : "goodsCount",
								menuDisabled : true,
								width : 80,
								align : "right"
							}, {
								header : "单位",
								dataIndex : "unitName",
								menuDisabled : true,
								width : 60
							}, {
								header : "库存金额",
								dataIndex : "goodsMoney",
								menuDisabled : true,
								width : 80,
								align : "right"
							}]
				});
		me.lookupGrid = lookupGrid;
		me.lookupGrid.on("itemdblclick", me.onOK, me);

		var wnd = Ext.create("Ext.window.Window", {
					title : "选择 - 商品",
					modal : true,
					width : 1200,
					height : 300,
					layout : "border",
					items : [{
								region : "center",
								xtype : "panel",
								layout : "fit",
								border : 0,
								items : [lookupGrid]
							}, {
								xtype : "panel",
								region : "south",
								height : 40,
								layout : "fit",
								border : 0,
								items : [{
											xtype : "form",
											layout : "form",
											bodyPadding : 5,
											items : [{
														id : "__editGoods",
														xtype : "textfield",
														fieldLabel : "商品",
														labelWidth : 50,
														labelAlign : "right",
														labelSeparator : ""
													}]
										}]
							}],
					buttons : [{
								text : "确定",
								handler : me.onOK,
								scope : me
							}, {
								text : "取消",
								handler : function() {
									wnd.close();
								}
							}]
				});

		wnd.on("close", function() {
					me.focus();
				});
		me.wnd = wnd;

		var warehouseId = null;
		var editWarehouse = Ext.getCmp(me.getEditWarehouseName());
		if (editWarehouse) {
			warehouseId = editWarehouse.getIdValue();
		}

		var editName = Ext.getCmp("__editGoods");
		editName.on("change", function() {
			var store = me.lookupGrid.getStore();
			Ext.Ajax.request({
						url : PSI.Const.BASE_URL + "Home/Goods/queryDataWithInventory",
						params : {
							queryKey : editName.getValue(),
							warehouseId : warehouseId
						},
						method : "POST",
						callback : function(opt, success, response) {
							store.removeAll();
							if (success) {
								var data = Ext.JSON
										.decode(response.responseText);
								store.add(data);
								if (data.length > 0) {
									me.lookupGrid.getSelectionModel().select(0);
									editName.focus();
								}
							} else {
								PSI.MsgBox.showInfo("网络错误");
							}
						},
						scope : this
					});

		}, me);

		editName.on("specialkey", function(field, e) {
					if (e.getKey() == e.ENTER) {
						me.onOK();
					} else if (e.getKey() == e.UP) {
						var m = me.lookupGrid.getSelectionModel();
						var store = me.lookupGrid.getStore();
						var index = 0;
						for (var i = 0; i < store.getCount(); i++) {
							if (m.isSelected(i)) {
								index = i;
							}
						}
						index--;
						if (index < 0) {
							index = 0;
						}
						m.select(index);
						e.preventDefault();
						editName.focus();
					} else if (e.getKey() == e.DOWN) {
						var m = me.lookupGrid.getSelectionModel();
						var store = me.lookupGrid.getStore();
						var index = 0;
						for (var i = 0; i < store.getCount(); i++) {
							if (m.isSelected(i)) {
								index = i;
							}
						}
						index++;
						if (index > store.getCount() - 1) {
							index = store.getCount() - 1;
						}
						m.select(index);
						e.preventDefault();
						editName.focus();
					}
				}, me);

		me.wnd.on("show", function() {
					editName.focus();
					editName.fireEvent("change");
				}, me);
		wnd.show();
	},

	onOK : function() {
		var me = this;
		var grid = me.lookupGrid;
		var item = grid.getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			return;
		}

		var data = item[0].getData();

		me.wnd.close();
		me.focus();
		me.setValue(data.code);
		me.focus();

		if (me.getParentCmp() && me.getParentCmp().__setGoodsInfo) {
			me.getParentCmp().__setGoodsInfo(data)
		}
	}
});