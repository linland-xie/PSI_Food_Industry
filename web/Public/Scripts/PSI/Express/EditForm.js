/**
 * 运单变更 - 运单管理
 */
Ext.define("PSI.Express.EditForm", {
			extend : "Ext.window.Window",

			config : {
				parentForm : null,
				entity : null
			},

			/**
			 * 初始化组件
			 */
			initComponent : function() {
				var me = this;

				var buttons = [];

				var btn = {
					text : "变更",
					formBind : true,
					iconCls : "PSI-button-ok",
					handler : function() {
						me.onOK(false);
					},
					scope : me
				};
				buttons.push(btn);

				var btn = {
					text : "取消",
					handler : function() {
						me.close();
					},
					scope : me
				};
				buttons.push(btn);

				Ext.apply(me, {
							title : "运单变更",
							modal : true,
							resizable : false,
							onEsc : Ext.emptyFn,
							width : 600,
							height : 620,
							layout : "fit",
							listeners : {
								show : {
									fn : me.onWndShow,
									scope : me
								},
								close : {
									fn : me.onWndClose,
									scope : me
								}
							},
							items : [{
								id : "editForm",
								xtype : "form",
								layout : {
									type : "table",
									columns : 1
								},
								height : "100%",
								bodyPadding : 5,
								defaultType : 'textfield',
								fieldDefaults : {
									labelWidth : 70,
									labelAlign : "right",
									labelSeparator : "",
									msgTarget : 'side',
									width : 570,
									margin : "5"
								},
								items : [{
											id : "editBillId",
											xtype : "hidden",
											name : "id",
											value : me.getEntity().get("id")
										}, {
											id : "editRef",
											fieldLabel : "运单单号",
											name : "ref",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editStatus",
											xtype : "combo",
											allowBlank : false,
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "status",
											queryMode : "local",
											editable : false,
											valueField : "id",
											fieldLabel : "状态",
											store : Ext.create(
													"Ext.data.ArrayStore", {
														fields : ["id", "text"],
														data : [
																["100", "待收件"],
																["200", "已收件"],
																["300",
																		"货物达到仓库"],
																["400", "派送中"],
																["500", "已签收"]]
													})
										}, {
											id : "editRecName",
											fieldLabel : "收镖人姓名",
											allowBlank : false,
											blankText : "没有输入收镖人姓名",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "recName",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editDestPlace",
											fieldLabel : "目的地",
											allowBlank : false,
											blankText : "没有输入目的地",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "destPlace",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editRecAddress",
											fieldLabel : "收镖人地址",
											allowBlank : false,
											blankText : "没有输入收镖人地址",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "recAddress",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editRecTel",
											fieldLabel : "收镖人电话",
											allowBlank : false,
											blankText : "没有输入收镖人电话",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "recTel",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editCustomer",
											xtype : "psi_customerfield",
											fieldLabel : "选择托镖人",
											allowBlank : false,
											blankText : "没有输入托镖人",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											},
											callbackFunc : me.__setCustomerExtData
										}, {
											id : "editCustomerId",
											xtype : "hidden",
											name : "customerId"
										}, {
											id : "editStartPlace",
											fieldLabel : "始发地",
											allowBlank : false,
											blankText : "没有输入始发地",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "startPlace",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editSendAddress",
											fieldLabel : "托镖人地址",
											allowBlank : false,
											blankText : "没有输入托镖人地址",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "sendAddress",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editSendTel",
											fieldLabel : "托镖人电话",
											allowBlank : false,
											blankText : "没有输入托镖人电话",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "sendTel",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editCargo",
											fieldLabel : "镖物",
											allowBlank : false,
											blankText : "没有输入镖物",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "cargo",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editFragile",
											fieldLabel : "勿压易碎",
											xtype : "combo",
											queryMode : "local",
											editable : false,
											valueField : "id",
											store : Ext.create(
													"Ext.data.ArrayStore", {
														fields : ["id", "text"],
														data : [["0", "非易碎物"],
																["1", "易碎物"]]
													}),
											name : "fragile",
											value : "0",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editFreight",
											fieldLabel : "运费",
											xtype : "numberfield",
											hideTrigger : true,
											allowBlank : false,
											allowBlank : false,
											blankText : "没有输入运费",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "freight",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editPayment",
											fieldLabel : "付款方式",
											allowBlank : false,
											blankText : "没有输入付款方式",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											xtype : "combo",
											queryMode : "local",
											editable : false,
											valueField : "id",
											store : Ext.create(
													"Ext.data.ArrayStore", {
														fields : ["id", "text"],
														data : [["10", "现金"],
																["20", "到付"],
																["30", "月结"]]
													}),
											name : "payment",
											value : "10",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editClaimValue",
											fieldLabel : "申明价值",
											xtype : "numberfield",
											hideTrigger : true,
											name : "claimValue",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editCargoMoney",
											fieldLabel : "代收货款",
											xtype : "numberfield",
											hideTrigger : true,
											name : "cargoMoney",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editMemo",
											fieldLabel : "备注",
											name : "memo",
											listeners : {
												specialkey : {
													fn : me.onLastEditSpecialKey,
													scope : me
												}
											}
										}],
								buttons : buttons
							}]
						});

				me.callParent(arguments);

				me.__editorList = ["editRef", "editRecName", "editDestPlace",
						"editRecAddress", "editRecTel", "editCustomer",
						"editStartPlace", "editSendAddress", "editSendTel",
						"editCargo", "editFragile", "editFreight",
						"editPayment", "editClaimValue", "editCargoMoney",
						"editMemo"];
			},

			/**
			 * 保存
			 */
			onOK : function() {
				var me = this;
				var id = Ext.getCmp("editBillId").getValue();
				if (!id) {
					PSI.MsgBox.showInfo("运单的Id没有值，无法保存数据");
					return;
				}

				Ext.getCmp("editCustomerId").setValue(Ext
						.getCmp("editCustomer").getIdValue());

				var f = Ext.getCmp("editForm");
				var el = f.getEl();
				var sf = {
					url : PSI.Const.BASE_URL + "Home/Express/editExpressBill",
					method : "POST",
					success : function(form, action) {
						el.unmask();

						PSI.MsgBox.tip("数据保存成功");
						var id = me.getEntity().get("id");
						me.getParentForm().refreshEditResult(id);
						me.close();
					},
					failure : function(form, action) {
						el.unmask();
						PSI.MsgBox.showInfo(action.result.msg, function() {
									Ext.getCmp("editRecName").focus();
								});
					}
				};

				PSI.MsgBox.confirm("请确认是否保存数据?", function() {
							el.mask(PSI.Const.SAVING);
							f.submit(sf);
						});
			},

			onWndClose : function() {
				var me = this;
			},

			onWndShow : function() {
				Ext.getCmp("editRecName").focus();

				var me = this;

				var el = me.getEl();
				el.mask(PSI.Const.LOADING);
				Ext.Ajax.request({
							url : PSI.Const.BASE_URL
									+ "Home/Express/expressBillInfo",
							params : {
								id : me.getEntity().get("id")
							},
							method : "POST",
							callback : function(options, success, response) {
								if (success) {
									var data = Ext.JSON
											.decode(response.responseText);

									Ext.getCmp("editRef").setValue(data.ref);
									Ext.getCmp("editStartPlace")
											.setValue(data.startPlace);
									Ext.getCmp("editDestPlace")
											.setValue(data.destPlace);
									Ext.getCmp("editStatus")
											.setValue(data.status);

									Ext.getCmp("editCustomer")
											.setIdValue(data.customerId);
									Ext.getCmp("editCustomer")
											.setValue(data.customerName);
									Ext.getCmp("editRecName")
											.setValue(data.recName);
									Ext.getCmp("editRecAddress")
											.setValue(data.recAddress);
									Ext.getCmp("editRecTel")
											.setValue(data.recTel);
									Ext.getCmp("editSendAddress")
											.setValue(data.sendAddress);
									Ext.getCmp("editSendTel")
											.setValue(data.sendTel);
									Ext.getCmp("editCargo")
											.setValue(data.cargo);
									Ext.getCmp("editFragile")
											.setValue(data.isFragile);
									Ext.getCmp("editFreight")
											.setValue(data.freight);
									Ext.getCmp("editPayment")
											.setValue(data.payment);
									Ext.getCmp("editClaimValue")
											.setValue(data.claimValue);
									Ext.getCmp("editCargoMoney")
											.setValue(data.cargoMoney);
									Ext.getCmp("editMemo").setValue(data.memo);
								}

								el.unmask();
							}
						});
			},

			__setCustomerExtData : function(data) {
			},

			onEditSpecialKey : function(field, e) {
				if (e.getKey() === e.ENTER) {
					var me = this;
					var id = field.getId();
					for (var i = 0; i < me.__editorList.length; i++) {
						var editorId = me.__editorList[i];
						if (id === editorId) {
							var edit = Ext.getCmp(me.__editorList[i + 1]);
							edit.focus();
							edit.setValue(edit.getValue());
						}
					}
				}
			},

			onLastEditSpecialKey : function(field, e) {
				if (e.getKey() === e.ENTER) {
					var f = Ext.getCmp("editForm");
					if (f.getForm().isValid()) {
						this.onOK();
					}
				}
			}
		});